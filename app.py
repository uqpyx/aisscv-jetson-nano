from ultralytics import YOLO
import asyncio
import websockets
import cv2

# Set the port for the WebSocket server
PORT = 8765

async def sender(websocket, path):
    """
    Continuously send YOLO prediction results to the connected client via WebSockets.

    Args:
        websocket: The active WebSocket connection.
        path: The connection path (not used in this context).
    """
    try:
        # Notify that a client has connected and inference is starting
        print(f"🔵️ Client connected ...")
        print(f"👁️  Starting inference ...")
        
        while True:
            for r in results:
                # Convert the prediction to JSON format
                message = r.tojson()
                
                # Send the message to the client
                await websocket.send(message)

                # Introduce a short delay to control the sending rate
                await asyncio.sleep(0.03)

    except websockets.ConnectionClosed:
        # Notify when the client disconnects
        print(f"🔴️ Client disconnected ...")
    except Exception as e:
        # Handle any other exceptions
        print(f"An error occurred: {e}")
    finally:
        # Cleanup resources
        cv2.destroyAllWindows()

		    
if __name__ == "__main__":
    # Load the YOLO model from the given file
    model = YOLO("best.pt")
    
    # Get prediction results. This continuously streams for source "1" with a confidence threshold of 0.5
    results = model.predict(source="1", conf=0.5, show=True, stream=True, device="0")

    # Setup and start the WebSocket server
    loop = asyncio.get_event_loop()
    server = websockets.serve(sender, "0.0.0.0", PORT)
    
    # Notify that the WebSocket server has started and is waiting for a client connection
    print(f"✅️ Websocket opened on port {PORT} ...")
    print("⏲️  Waiting for client to connect...")
    
    # Start the server and keep it running
    loop.run_until_complete(server)
    loop.run_forever()

